import React from 'react';
import style from './Navbar.module.scss'
const Navbar = ( props: { children: string | number | boolean | {} | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactNodeArray | React.ReactPortal | null | undefined; rColor: string | number | boolean | {} | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactNodeArray | React.ReactPortal | null | undefined; } ) => {
    return (
      <nav className={style.navbar}>
        Nav
        {props.children}
        {props.rColor}
      </nav>
    )
  }
  
export default Navbar