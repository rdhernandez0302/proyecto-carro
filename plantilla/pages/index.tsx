import React, { useState } from 'react';
import Cookies from 'js-cookie';
import Router from 'next/router';
import {
  getAppCookies,
  verifyToken
} from '@middleware/util';

export async function getServerSideProps(context: { req: any; }) {
  const { req } = context;
 
  const { token } = getAppCookies(req);
  const profile = token ? verifyToken(token.split(' ')[1]) : '';
  return {
    props: {
      profile,
    },
  };
}

const FORM_DATA_LOGIN = {
  email: {
    value: '',
    label: 'Email',
    min: 10,
    max: 36,
    required: true,
  },
  password: {
    value: '',
    label: 'Password',
    min: 6,
    max: 36,
    required: true,
  },
};

const Index = ( props: { profile: any; } ) => {
    const {  profile } = props;
    const [stateFormData, setStateFormData] = useState(FORM_DATA_LOGIN);
    function onChangeHandler(e: { currentTarget: { name: any; value: any; }; }) {
      const { name, value } = e.currentTarget;

      setStateFormData({
        ...stateFormData,
        [name]: {
          value,
        },
      });
    }
    async function onSubmitHandler(e: { preventDefault: () => void; }) {
      e.preventDefault();
      let data = { 
        email: stateFormData.email.value,
        password: stateFormData.password.value
      };
  
      const loginApi:any = await fetch(`/api/auth`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).catch(error => {
        console.error('Error:', error);
      });

      let result = await loginApi.json();
      if (result.success && result.token) {
        Cookies.set('token', result.token);
        Router.push('/cart');
      } else {
        console.log(result);
      }
    }

    return (
      <>
        <h1>Bienvenido: {profile.email}</h1>
        <form className="form-login card" method="POST" >
          <div className="form-group">
            <h2>Login</h2>
          </div>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              className="form-control"
              type="text"
              id="email"
              name="email"
              placeholder="Email"
              onChange={onChangeHandler}
              value={stateFormData.email.value}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              className="form-control"
              type="password"
              id="password"
              name="password"
              placeholder="Password"
              onChange={onChangeHandler}
              value={stateFormData.password.value}
            />
          </div>
          <div>
            <button type="button" className="btn btn-block btn-warning" onClick={onSubmitHandler}>
              Login
            </button>
          </div>
        </form>
      </>
    )
}
  
export default Index
