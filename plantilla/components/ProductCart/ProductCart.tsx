import React from 'react';
import style from './ProductCart.module.scss'
const ProductCart = () => {
    return (
      <div className={style.card}>
        <div className={style.cardHeader}>
          Título
        </div>
        <div className={style.cardBody}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad dolore facilis ipsum dicta et, sapiente animi vero voluptatibus. Dolores quia tenetur voluptatem aperiam doloremque vel fugiat. Ab quae vel accusantium.
        </div>
        <div className={style.cardFooter}>
          Footer
        </div>
      </div>
    )
  }
  
export default ProductCart